using System;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.ConfigurationModels
{
    public class RabbitMqConfiguration 
    {
        private RabbitMqConfigurationOptions _configuration;
        private IConnection _connection;
        public RabbitMqConfiguration(IOptions<RabbitMqConfigurationOptions> configurations)
        {
            _configuration = new RabbitMqConfigurationOptions();
            _configuration = configurations.Value;
            _connection = GetConnection();
        }

        public string GetQueueName(){
            return _configuration.QueueName;
        }
        public IConnection GetConnection()
        {
             try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _configuration.HostName,
                    UserName = _configuration.UserName,
                    Password = _configuration.Password,
                    VirtualHost = _configuration.VirtualHost
                };
                var conn = factory.CreateConnection();
                return factory.CreateConnection();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine($"Problems with connection creation QueueAdministrationGateway {ex.Message}");
                throw;
            }
        }

    }
}