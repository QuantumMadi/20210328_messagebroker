using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.Administration.DataAccess;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.Administration.WebHost.RabbitMqConfiguration
{
    public class PartnerManagerPromocodeNotificationListener : BackgroundService
    {

        private IConnection _connection;
        private IModel _channel;
        private string _queueName;
        private IServiceProvider _provider;
        public PartnerManagerPromocodeNotificationListener(IOptions<RabbitMqConfigurationOptions> configuration,
        IServiceProvider provider)
        {
            _provider = provider;
            var factory = new ConnectionFactory
            {
                HostName = configuration.Value.HostName,
                UserName = configuration.Value.UserName,
                Password = configuration.Value.Password,
                VirtualHost = configuration.Value.VirtualHost
            };
            _queueName = configuration.Value.QueueName;
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: configuration.Value.QueueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var guid = JsonConvert.DeserializeObject<Guid>(content);
                try
                {
                    using (var scope = _provider.CreateScope())
                    {
                        var service = scope.ServiceProvider.GetService<DataContext>();
                        var employee = service.Employees.First(x => x.Id == guid);
                        if (employee != null)
                        {
                            employee.AppliedPromocodesCount++;
                        }
                        service.SaveChanges();
                    }                    

                }
                catch (System.Exception)
                {
                    
                }


                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume(_queueName, false, consumer);

            return Task.CompletedTask;
        }
    }
}