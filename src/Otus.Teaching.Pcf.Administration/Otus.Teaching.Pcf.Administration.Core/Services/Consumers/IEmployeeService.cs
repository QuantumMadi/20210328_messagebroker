using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public interface IEmployeeService
    {
         Task UpdatePromocodes(Guid id);
    }
}